package app;

import java.math.BigInteger;

public class Amount {

    private BigInteger amount;
    private BigInteger max = BigInteger.valueOf(Integer.MAX_VALUE);
    private BigInteger min = BigInteger.ZERO;

    public Amount(String amount) {
        if (amount ==  null)
            throw new IllegalArgumentException("Amount is null");

        BigInteger newAmount = new BigInteger(amount);
        if (newAmount.compareTo(min) <= 0)
            throw new IllegalArgumentException("Amount below minimum");
        if (newAmount.compareTo(max) >= 0)
            throw new IllegalArgumentException("Amount above max");
        this.amount = new BigInteger(amount);
    }

    public BigInteger getAmount() {
        return amount;
    }
}

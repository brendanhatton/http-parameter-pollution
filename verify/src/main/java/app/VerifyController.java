package app;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import app.exception.BadRequestException;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriBuilderFactory;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
public class VerifyController {

    @Value("${paymenturl}")
    private String paymentUrl;

    @PostMapping("/")
    public String res(HttpServletRequest request) {
        try {
            ActionType action = ActionType.valueOf(request.getParameter("action").toUpperCase());
            Amount amount = new Amount(request.getParameter("amount"));
            if (ActionType.TRANSFER.equals(action)) {
                System.out.println("Verify Controller: Going to transfer $"+amount);
                RestTemplate restTemplate = new RestTemplate();
                String fakePaymentUrl = this.paymentUrl;  //Internal fake payment micro-service

                UriComponents uriComponents = UriComponentsBuilder.newInstance()
                        .scheme("http").host(fakePaymentUrl)
                        .queryParam("action", action.name().toLowerCase())
                        .queryParam("amount", amount.getAmount())
                        .build();
                ResponseEntity<String> response
                    = restTemplate.getForEntity(
                            uriComponents.toUriString(),
                            String.class);
                return response.getBody();
            } else if (ActionType.WITHDRAW.equals(action)) {
                return "Verify Controller: Sorry, you can only make transfer";
            } else {
                return "Verify Controller: You must specify action: transfer or withdraw";
            }
        } catch(RuntimeException ex) {
            throw new BadRequestException();
        }
    }

}

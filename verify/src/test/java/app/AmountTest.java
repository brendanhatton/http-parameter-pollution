package app;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class AmountTest {

    @Test
    void shouldNotAllowNullAmount() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Amount(null);
        });
    }

    @Test
    void shouldNotAllowInvalidAmountStrings() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Amount("haha");
        });
    }

    @Test
    void shouldOnlyAllowAmountsGreatherThanMinimum() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Amount("-1");
        });
    }

    @Test
    void shouldOnlyAllowAmountsLessThanMaximum() {
        assertThrows(IllegalArgumentException.class, () -> {
            BigInteger tooLarge = BigInteger.valueOf(Integer.MAX_VALUE).add(BigInteger.ONE);
            new Amount(tooLarge.toString());
        });
    }
}
